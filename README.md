# Onboarding Vtex

## Catálogo

### Categorias

Dentro de categorias temos a opção Incluir
e realizando essa ação criamos um Departamento, 
quando realizamos o Incluir dentro de um departamento criamos uma Categoria,
a ação Incluir dentro de Categoria criamos subcategorias.
Por convenção, é bom ter até três níveis em categorias, mais do que isso filtra demais e fica confuso e ruim para o cliente encontrar.

Em categorias podemos realizar ações para a raíz e para cada um dos níveis subsequentes

Essas ações são:

* **Alterar:** Editar Funcionalidades dentro da categoria;
* **Incluir Similar:** Mostra os produtos da categoria em questão em outra, sem as informações de grupo, Campo(Produto), Campo(SKU)
* **Incluir:** para criar uma categoria, ou subcategoria;
* **Grupo:** Seções dentro de uma página de produtos;
* **Campo(Produto):** Dentro de Grupo temos campos, o campo (produto) são as características do produto
exemplo camiseta: um campo de produto seria por exemplo a composição (100% algodão, por exemplo), é uma especificação do produto;
* **Campo(SKU):** variações do produto, usando o exemplo da camiseta, a variação do produto seria tamanho(p, m, g), cor (branca, preta), etc
* **Avaliação:** avaliação da categorias
* **Faixa de preço:** criar filtragem por faixa de preço


### Produtos e SKUs

Um produto precisa ter pelo menos um sku para ficar ativo, a imagem relacionada ao produto vai ser sempre a imagem do primeiro sku ativo.
SKU: Variações do produto (cor tamo, etc)

### Marcas

Obrigatória para todos os produtos
Nessa opção cadastro de marcas (Nome, detalhes, imagem da marca)

### Importação e exportação

importação e exportação de arquivos (planilhas, imagens, etc) para os produtos
O intuito desse módulo é fazer alterações em massa, alterações em apenas um produto é melhor fazer direto em produto


### Anexos

Customização no produto:
Ex: embalagem para presente, especificações onde vai aparecer essa opção, se vai ter taxa ou não e qual a taxa

regras de assembly options: para colocar opções de acrescimos


### Coleções

Para criar coleções, colocando produtos, categorias
É bem visual, onde podemos selecionar, deselecionar, remover, arrastar
Podemos editar configurações como nome, data (como exemplo uma coleção que vai ser usada em uma promoção de black friday, ou de lançamento já pode deixar configurado para esse dia específico)


## Preços

### Lista de Preços

Precificação do SKUs.
Podemos fazer precificação de categorias inteiras fazendo a importação e exportação da planilha com as mudanças de preços.
Tabelas de preços: políticas comerciais (politicas comerciais de preços para países diferentes, por exemplo).

### Regras de preço

Descontos ou acréscimos dependendo de alguma regra, por exemplo: pagamento á vista, pagamento no boleto, etc

### Arredondamento de preços

Quantos digitos serão arredondados, e qual o metodo de arredondamento (90, 99, 00)

### Configurações

Markup padrão para novos preços: Para colocar o lucro que quer ter nos produtos (%)

Para colocar limite de variação de preço: quando vai ser colocado descontos diferentes em diferentes tipos de produtos, em um 30% e em outro 20%, mas por acaso tem um produto especifico que se encaixa nos dois tipos, aí ele não fica com 50% de desconto, fica com a porcentagem máxima de desconto que foi colocada aqui, por exemplo 35%.

## Promoções e taxas

### Audiências de campanha

São containers de promoções com detalhamento

### Promoções

Criar promoções, que podem ser:

* **Promoção Regular:** Desconto no produto, ou no frete (é a mais comum)
* **Compre Junto:** Comprando dois produtos específicos juntos ganha desconto
* **Leve mais por menos:** leve 5 pague 4, por exemplo
* **Desconto progressico:** quanto mais eu acrescento produto, maior o desconto
* **Compre e ganhe**: compre um produto e ganhe mais um outro
* **Promoção de campanha:** promoções com publicos alvos

### Cupons

Cupom é um código que poderá ser colocado no carrinho de compras para ativar uma promoção
Criar de cupom, seu nome, restrições e limitações

### Taxas

Criar taxas, para colocar um imposto por exemplo.
Nome da taxa, descrição, validade, tipo e valor
Quais itens vão receber essa taxa (produto, categoria, coleção)
e mais algumas informações especificas que podem ser aplicadas em relação a taxas(diferença por estado, etc)

## Configurações de conta

### Perfis de acesso

Permissões de acesso para diferentes times

### Usuários

Criar usuário e de que perfil pertence(time)

### Conta

Informações da conta da Vtex e adicionamento de lojas, segurança (chave de APIs, só o suário mestre mexe aqui) e logotipo

### Autenticação

Configurações das formas que o usuário pode usar para logar (Código de acesso, senha, Google, Facebook)

### Aplicativos

Opções de aplicativos para instalar na loja, para trazer mais funcionalidades a loja


## Pedidos

### Gerencimento de pedidos


* **Todos od pedidos:** Ver os pedidos e status
* **Configurações:** 
* ------ Carrinho -> quantidade mínima de itens para realizar compra/pedido, valor mínimo, casas decimais;
* ------ Pedidos -> permissões ao clientes para alteração no pedido





































